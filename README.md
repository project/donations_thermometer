CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Features
 * Backwards Compatibility
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers



INTRODUCTION
------------
This documentation is for 7.x-2.x.

This module generates a "donation thermometer" which shows progress of an
organizations donation.



FEATURES
--------

 * Classy.org or Manual data feed.
 * Multiple meters.
 * Horizontal / Vertical orientation of the meters.
 * Preset meter colors.
 * Styling customizable via CSS3.


BACKWARDS COMPATIBILITY
-----------------------
* Panels not yet tested with D7.x-2.x
* Styling vastly different between D7.x-2.x and D7.x-1.x


REQUIREMENTS
------------

This module does not require any additional modules. It is designed to use
HTML5 and CSS3.

It does support the Panels module.



INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
 https://drupal.org/documentation/install/modules-themes/modules-7
 for further information.



CONFIGURATION
-------------

 * Configure user permissions in Administration » People » Permissions:

 * Configure the number of Thermometer blocks you will need.
   - Administration » Configuration and modules » Donation Thermometer.

 * Configure individual blocks.
   - Navigate to Structure » Blocks

   - Click on configure for the block you wish to edit.

   - Set the block type. Manual allows a user to manually adjust the goal and
   current level. Classy.org allows the block to get it's data from the
   Classy.org platform.



TROUBLESHOOTING
---------------

There are no none issues. If you find one please submit a request via the
project page:
[https://www.drupal.org/project/donations_thermometer](https://www.drupal.org/project/donations_thermometer)


FAQ
---

None so far so ask. Look at the TROUBLESHOOTING section.


Maintainers
-----------
Listed alphabetically by username.
* [lsolesen](https://www.drupal.org/u/lsolesen)
* [nazarioa](https://www.drupal.org/u/nazarioa)
* [sejtraav](https://www.drupal.org/u/sejtraav) -- Original Developer
